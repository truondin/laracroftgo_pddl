(define (problem dummy_use_lever)
    (:domain Lara_Croft_Go)
    (:objects
        tile1 tile2 tile3 tile4 tile5 tile6 tile8 - tile
        Lara - agent
        lever1 lever2 - lever
        tile7 - goal
    )

    (:init
        (path tile1 tile2)
        (path tile2 tile3)
        (path tile3 tile4)
        (path tile4 tile5)
        (path tile5 tile6)
        (path tile6 tile7)

        (path tile2 tile1)
        (path tile3 tile2)
        (path tile4 tile3)
        (path tile5 tile4)
        (path tile6 tile5)
        (path tile7 tile6)

        (forbidden_tile tile5)
        (forbidden_tile tile6)
        (forbidden_tile tile8)

        (at lever1 tile2)
        (at lever2 tile3)

        (lever_activates lever1 tile5)
        (lever_activates lever1 tile6)
        (lever_activates lever2 tile8)

        (at Lara tile1)
    )

    (:goal
        (and
            (at Lara tile7)
        )
    )
)

;  A     1
;  #     2
;###S 87536
;  #     4
;  G     G

(define (problem dummy_moving_trap)
    (:domain Lara_Croft_Go)
    (:objects
        tile1 tile2 tile3 tile4 tile5 tile6 tile7 tile8 - tile
        goal - goal
        Lara - agent
        
        tile6 tile3 tile5 tile7 tile8 - assigned_location
        saw - saw

    )

    (:init
        (path tile1 tile2)
        (path tile2 tile3)
        (path tile3 tile4)
        (path tile3 tile5)
        (path tile3 tile6)
        (path tile5 tile7)
        (path tile7 tile8)
        (path tile4 goal)
        
        (path tile2 tile1)
        (path tile3 tile2)
        (path tile4 tile3)
        (path tile5 tile3)
        (path tile6 tile3)
        (path tile7 tile5)
        (path tile8 tile7)
        (path goal tile4)
    
        (at Lara tile1)
        (at saw tile6)
        ; (is_guarded_tile tile3)
        (trap_guards_tile saw tile3)
    
    
        (= (move_trap_location saw tile6) 1)
        (= (guarded_tile_location_increase saw tile3) 1)
        (= (guarded_tile_location_decrease tile8 saw) 4000)

        (= (move_trap_location saw tile3) 2)
        (= (guarded_tile_location_increase saw tile5) 2)
        (= (guarded_tile_location_decrease tile6 saw) 2)

        (= (move_trap_location saw tile5) 3)
        (= (guarded_tile_location_increase saw tile7) 3)
        (= (guarded_tile_location_decrease tile3 saw) 3) 

        (= (move_trap_location saw tile7) 4)
        (= (guarded_tile_location_increase saw tile8) 4)
        (= (guarded_tile_location_decrease tile5 saw) 4)

        (= (move_trap_location saw tile8) 5)
        (= (guarded_tile_location_increase saw tile6) 5000)
        (= (guarded_tile_location_decrease tile7 saw) 5)
        
        (= (move_trap_step_count saw) 2)

        (= (move_trap_step_upper_limit saw) 5) 
        (= (move_trap_step_lower_limit saw) 1) 

        (do_increase saw)

    )

    (:goal
        (and
            (at Lara goal)

        )
    )
)

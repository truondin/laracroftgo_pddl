using SymbolicPlanners
using PDDL

function run(path)
    domain_path = "/Users/truondin/uni/bachelor/laracroftgo_pddl/domain.pddl"

    problem_first = "/Users/truondin/uni/bachelor/laracroftgo_pddl/problem5_moving-trap.pddl"
    problem_path = path


    println("\n---INITIALIZING DOMAIN---")
    domain = load_domain(domain_path)
    println(domain)
    sleep(1)

    try
        problem = load_problem(problem_path)
        println(problem)
        
        println("\n---INITIALIZING STATE---")
        state = initstate(domain, problem)
        println(state)
        
        println("\n---INITIALIZING GOAL---")
        goal = PDDL.get_goal(problem)
        goal_init = PDDL.get_goal(problem)
        
        # Construct A* planner with h_add heuristic
        println("\n---INITIALIZING PLANNER (A STAR)---")
        planner = AStarPlanner(HAdd())
        println(planner)
        
        println("\n---SOLVING LEVEL---")
        # Solve the problem using the planner
        try
            
        sol = @time planner(domain, state, goal)
        if satisfy(domain, sol.trajectory[end], goal)
                println("Problem solved for goal: ")
                println(goal_init)
            end
            
            println("\n---RESULT ACTIONS---")
            print("Number of actions: ")
            actions = collect(sol)
            println(size(actions, 1))
            println("")
            for ac in actions
                println(ac)
            end
            println("")
            
            print("")
            

        catch
            println("Could not solve the problem.")
        end
    catch
        println("ERROR ---> Invalid path to problem")
    end

end

if length(ARGS) == 1
    run(ARGS[1])
else
    println("Missing problem path arg")
end
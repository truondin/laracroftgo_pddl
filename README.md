# LaraCroftGO PDDL
This is a PDDL of a game Lara Croft Go. The domain implements game mechanics from first 2 chapters of the game. We also include example PDDL problems, which represents either a dummy level or an actual level from Lara Croft Go. The actual game-levels PDDL problem are in directory */game_levels*.

## Usage
Since the PDDL domain contains many advance PDDL features, not every planner can parse this PDDL. To run the PDDL we recommend to use [SymbolicPlanners.jl](https://github.com/JuliaPlanners/SymbolicPlanners.jl) as a domain-independent planner. This planner was used when evaluating the PDDL.


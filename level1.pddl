
(define (problem Entrance_1_3)
    (:domain Lara_Croft_Go)
    (:objects
        t1 t2 t4_a t4_b t5_a t5_b t6 t7 t8 t10 t11 t12 t13 - tile ; t11 a t12 jsou flip s t21 a t22 pro lever1
        t14 t15 t16 t18_a t18_b t19 t20 t21 t22 t23 t24 - tile
        t3 t9 t17_a t17_b - crack_tile
        g - goal

        lever1 - lever   ;na t10 a t19

        Lara - agent
    )

    (:init
        (path t1 t2)
        (path t2 t3)
        (path t3 t4_a)
        (path t3 t4_b)
        (path t4_a t5_a)
        (path t4_b t5_b)
        (path t5_a t6)
        (path t6 t7)
        (path t7 t8)
        (path t8 t9)
        (path t9 t10)
        (path t7 t11)
        (path t11 t12)
        (path t12 t13)
        (path t13 t14)
        (path t14 t15)
        (path t15 t16)
        (path t16 t17_a)
        (path t16 t17_b)
        (path t17_a t18_a)
        (path t17_b t18_b)
        (path t18_a t19)
        (path t18_b t19)
        (path t18_b t20)
        (path t13 t21)
        (path t21 t22)
        (path t22 t23)
        (path t23 t24)
        (path t24 g)
        
        (path t2 t1)
        (path t3 t2)
        (path t4_a t3)
        (path t4_b t3)
        (path t5_a t4_a)
        (path t5_b t4_b)
        (path t6 t5_a)
        (path t7 t6)
        (path t8 t7)
        (path t9 t8)
        (path t10 t9)
        (path t11 t7)
        (path t12 t11)
        (path t13 t12)
        (path t14 t13)
        (path t15 t14)
        (path t16 t15)
        (path t17_a t16)
        (path t17_b t16)
        (path t18_a t17_a)
        (path t18_b t17_b)
        (path t19 t18_a)
        (path t19 t18_b)
        (path t20 t18_b)
        (path t21 t13)
        (path t22 t21)
        (path t23 t22)
        (path t24 t23)
        (path g t24)
    
        (fall_path t9 t7)

        (at Lara t1)
        (at lever1 t10)
        (at lever1 t19)
        
        (forbidden_tile t11)
        (forbidden_tile t12)
        
        (lever_activates lever1 t11)
        (lever_activates lever1 t12)
        (lever_activates lever1 t21)
        (lever_activates lever1 t22)

        (flip_tile t11)
        (flip_tile t12)
        (flip_tile t21)
        (flip_tile t22)

        (is_crack_tile t3)
        (is_crack_tile t9)
        (is_crack_tile t17_a)
        (is_crack_tile t17_b)

    )

    (:goal
        (and
            (at Lara g)
        )
    )
)

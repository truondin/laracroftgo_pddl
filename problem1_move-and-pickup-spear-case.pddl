(define (problem dummy_move_and_pickup_spear)
    (:domain Lara_Croft_Go)
    (:objects
        tile1 - tile
        tile2 - tile
        tile3 - tile
        tile4 - tile
        tile5 - goal
        tile6 - tile
        tile7 - tile
        tile8 - tile
        tile9 - tile
        tile10 - tile
        tile11 - tile

        Lara - agent
        spear - spear
        lever - lever

    )

    (:init
        (at Lara tile1)
        ; (not (has Lara spear))
        (path tile1 tile2)
        (path tile2 tile3)
        (path tile3 tile4)
        (path tile4 tile5)

        (path tile2 tile1)
        (path tile3 tile2)
        (path tile4 tile3)
        (path tile5 tile4)

        (path tile2 tile6)
        (path tile6 tile2)
        (at spear tile6)

        (forbidden_tile tile4)
        (at lever tile3)
        (lever_activates lever tile4)
        (path tile3 tile7)
        (path tile7 tile8)
        (path tile8 tile9)
        (path tile9 tile10)
        (path tile10 tile11)
        (path tile11 tile5)

        (path tile7 tile3)
        (path tile8 tile7)
        (path tile9 tile8)
        (path tile10 tile9)
        (path tile11 tile10)
        (path tile5 tile11)
    )

    (:goal
        (and
            (at Lara tile5)
            ; (has Lara spear)
        )
    )
)
;  A     1
; #S    42
;  #     3
;  G     G

(define (problem dummy_close_range_kill)
    (:domain Lara_Croft_Go)
    (:objects
        tile1 tile2 tile3 tile4 - tile
        goal - goal
        Lara - agent
        snake - snake
        
        
    )

    (:init
        (path tile1 tile2)
        (path tile2 tile3)
        (path tile2 tile4)
        (path tile3 goal)
        
        (path tile2 tile1)
        (path tile3 tile2)
        (path tile4 tile2)
        (path goal tile3)
    
        (at Lara tile1)
        (at snake tile2)
        
        (trap_guards_tile snake tile3)
        ; (is_guarded_tile tile3)
    
        
    )

    (:goal
        (and
            (at Lara goal)
        )
    )
)

(define (domain Lara_Croft_Go)

    (:requirements 
        :strips 
        :adl 
        :typing 
        :fluents 
        :negative-preconditions 
        :equality 
    )

    (:types
        tile goal - location
        assigned_location - tile
        crack_tile - tile

        moveable trap item - object
        move_trap animal - trap

        agent move_trap - moveable

        saw spider salamander - move_trap
        snake salamander spider - animal

        lever careable - item
        spear - careable
    )

    (:constants

    )

    (:predicates
        (agent_dead ?a - agent)

        (at ?o - object ?l - location)
     
        (path ?l1 - location ?l2 - location)
     
        (has ?a - agent ?careable - careable)
     
        (forbidden_tile ?t - tile)
        (flip_tile ?t - tile)
        (is_cracked ?t - crack_tile)
        (is_destroyed_tile ?t2 - tile)
        (is_guarded_tile ?t - tile)
     
        (fall_path ?l1 - location ?l2 - location)
        (lever_activates ?l - lever ?t - tile)

        (is_crack_tile ?t - tile)
    
        (animal_dead ?a - animal)
        (trap_guards_tile ?t - trap ?t - tile)
        (spear_throw_line ?l1 - location ?l2 - location)
        
        (active ?trap - trap)
        (do_increase ?move_trap)
    )

    (:functions
        (move_trap_step_count ?trap - move_trap) - number
        (move_trap_step_upper_limit ?trap - move_trap) - number
        (move_trap_step_lower_limit ?trap - move_trap) - number
        (move_trap_location ?trap - move_trap ?loc - assigned_location) - number

        (guarded_tile_location_increase ?trap - move_trap ?tile - assigned_location) - number
        (guarded_tile_location_decrease ?tile - assigned_location ?trap - move_trap ) - number
    )

    (:action crack_tile
        :parameters (?a - agent ?t1 - tile ?t2 - crack_tile)
        :precondition (and
            (at ?a ?t1)
            (path ?t1 ?t2)
            (not (is_cracked ?t2))
            ; (not (is_guarded_tile ?t2))
            (not (forbidden_tile ?t2))

            (forall (?trap - trap) (not (trap_guards_tile ?trap ?t2)))
        )
        :effect (and
            (is_cracked ?t2)
            (at ?a ?t2)Í›
            (not (at ?a ?t1))
        )
    )

    (:action destroy_tile
        :parameters (?a - agent ?t1 - tile ?t2 - crack_tile)
        :precondition (and
            (at ?a ?t1)
            (is_cracked ?t2)
            (path ?t1 ?t2)
            (not (forbidden_tile ?t2))
        )
        :effect (and
            (at ?a ?t2)
            (is_destroyed_tile ?t2)
            (not (is_cracked ?t2))
            (not (at ?a ?t1))
        )
    )

    (:action fall_through
        :parameters (?a - agent ?t1 - crack_tile ?t2 - tile)
        :precondition (and
            (at ?a ?t1)
            (is_destroyed_tile ?t1)
            (fall_path ?t1 ?t2)
            (not (forbidden_tile ?t2))
        )
        :effect (and
            (at ?a ?t2)
            (not (at ?a ?t1))
        )
    )

    (:action fall_to_death
        :parameters (?a - agent ?t1 - crack_tile ?t2 - tile)
        :precondition (and
            (at ?a ?t1)
            (is_destroyed_tile ?t1)
            (or (not (fall_path ?t1 ?t2)) (not (forbidden_tile ?t2)))
        )
        :effect (and
            (agent_dead ?a)
            (not (at ?a ?l1))
        )
    )
    
    (:action move_and_kill_animal_close
        :parameters (?a - agent ?l1 - tile ?l2 - tile ?guarded_tile - tile ?animal - animal)
        :precondition (and 
            (at ?a ?l1)
            (at ?animal ?l2)
            
            (path ?l1 ?l2)
            
            (not (is_destroyed_tile ?l1))
            (not (forbidden_tile ?l2))
            (not (is_crack_tile ?l2))
            
            (not (animal_dead ?animal))
            (trap_guards_tile ?animal ?guarded_tile)
            ; (is_guarded_tile ?guarded_tile)

            (forall (?trap - trap) 
                (and (not (trap_guards_tile ?trap ?l1)) (not (trap_guards_tile ?trap ?l2)))
            )
            
        )
        :effect (and
            (animal_dead ?animal)
            (not (at ?animal ?l2))
            (not (trap_guards_tile ?animal ?guarded_tile))
            ; (not (is_guarded_tile ?guarded_tile))
            
            (not (at ?a ?l1))
            (at ?a ?l2)
        )
    )
    
    (:action move_to_non_goal
        :parameters (?a - agent ?l1 - tile ?l2 - tile)
        :precondition (and
            (at ?a ?l1)
            (path ?l1 ?l2)
            (not (is_destroyed_tile ?l1))
            (not (forbidden_tile ?l2))
            (not (is_crack_tile ?l2))
            (not (is_cracked ?l2))
            
            (forall (?animal - animal) 
                (not (at ?animal ?l2) (not (trap_guards_tile ?animal ?l1)))
            )
            (forall (?trap - trap) 
                (and (not (at ?trap ?l2)) (not (trap_guards_tile ?trap ?l2)))
            )
        )
        :effect (and
            (not (at ?a ?l1))
            (at ?a ?l2)

; musim se posunout v case protoze v effectu se increase udela az po dokonceni vseho, takze assign dalsiho tile musi bude zpozdeny
            
            
            (forall (?trap - move_trap)
                (and
                    (when (do_increase ?trap)
                        (increase (move_trap_step_count ?trap) 1)
                    )
                    (when (not (do_increase ?trap))
                        (decrease (move_trap_step_count ?trap) 1)
                    )
                    (when (= (move_trap_step_count ?trap) (move_trap_step_upper_limit ?trap))
                        (and
                            (not (do_increase ?trap))
                            (decrease (move_trap_step_count ?trap) 1)
                        )
                    )
                    (when (= (move_trap_step_count ?trap) (move_trap_step_lower_limit ?trap))
                        (and
                            (do_increase ?trap)
                            (increase (move_trap_step_count ?trap) 1)
                        )    
                    )

                    (forall (?as_loc - assigned_location) 
                        (and
                            (when (at ?trap ?as_loc)
                                (not (at ?trap ?as_loc))
                            )
                            (when (= (move_trap_step_count ?trap) (move_trap_location ?trap ?as_loc))
                                (at ?trap ?as_loc)
                            )

                            (when (and (do_increase ?trap) (= (move_trap_step_count ?trap) (guarded_tile_location_increase ?trap ?as_loc)))
                                (trap_guards_tile ?trap ?as_loc)
                            )
                            (when (and (do_increase ?trap) (not (= (move_trap_step_count ?trap) (guarded_tile_location_increase ?trap ?as_loc))))
                                (not (trap_guards_tile ?trap ?as_loc))
                            )

                            (when (and (not (do_increase ?trap)) (= (move_trap_step_count ?trap) (guarded_tile_location_decrease ?as_loc ?trap)))
                                ; (is_guarded_tile ?as_loc)
                                (trap_guards_tile ?trap ?as_loc)
                            )
                            (when (and (not (do_increase ?trap)) (not (= (move_trap_step_count ?trap) (guarded_tile_location_decrease ?as_loc ?trap))))
                                (not (trap_guards_tile ?trap ?as_loc))
                            )

                            ; rohy pohybujicich se traps
                            (when (and (do_increase ?trap) (= (move_trap_step_count ?trap) (move_trap_step_upper_limit ?trap))
                                     (= (move_trap_step_count ?trap) (guarded_tile_location_decrease ?as_loc ?trap)))
                                (trap_guards_tile ?trap ?as_loc)
                            )
                            (when (and (do_increase ?trap) (= (move_trap_step_count ?trap) (move_trap_step_lower_limit ?trap)) 
                                    (= (move_trap_step_count ?trap) (guarded_tile_location_increase ?trap ?as_loc)))
                                (trap_guards_tile ?trap ?as_loc)
                            )
                        )
                    )
                )
            )
        )
    )

    (:action move_to_goal
        :parameters (?a - agent ?l1 - tile ?l2 - goal)
        :precondition (and
            (at ?a ?l1)
            (path ?l1 ?l2)
            (not (is_destroyed_tile ?l1))
        )
        :effect (and
            (not (at ?a ?l1))
            (at ?a ?l2)
        )
    )

    (:action pickup_spear
        :parameters (?a - agent ?l - location ?s - spear)
        :precondition (and
            (at ?a ?l)
            (at ?s ?l)
            (not (has ?a ?s))
        )
        :effect (and
            (has ?a ?s)
            (not (at ?s ?l))
        )
    )

    (:action throw_spear
        :parameters (?a - agent ?s - spear ?animal - animal ?l1 - location ?l2 - location ?guarded_tile - tile)
        :precondition (and 
            (at ?a ?l1)
            (at ?animal ?l2)
            (spear_throw_line ?l1 ?l2)
            (has ?a ?s)
            (not (animal_dead ?animal))
            (trap_guards_tile ?animal ?guarded_tile)
            ; (is_guarded_tile ?guarded_tile)
        )
        :effect (and 
            (animal_dead ?animal)
            (not (at ?animal ?l2))
            (not (trap_guards_tile ?animal ?guarded_tile))
            ; (not (is_guarded_tile ?guarded_tile))
            
            (not (has ?a ?s))
        )
    )

    (:action use_lever
        :parameters (?a - agent ?lever - lever ?l - location ?tile - tile)
        :precondition (and
            (at ?a ?l)
            (at ?lever ?l)
            (forbidden_tile ?tile)
            (lever_activates ?lever ?tile)
            (not (flip_tile ?tile))
        )
        :effect 
            (forall (?tile - tile)
                (when (lever_activates ?lever ?tile) (not (forbidden_tile ?tile)))
        )
    )


    (:action use_lever_flip
        :parameters (?a - agent ?lever - lever ?l - location ?tile - tile ?tile2 - tile)
        :precondition (and
            (at ?a ?l)
            (at ?lever ?l)
            (forbidden_tile ?tile)
            (not (forbidden_tile ?tile2))
            (lever_activates ?lever ?tile)
            (lever_activates ?lever ?tile2)
            (flip_tile ?tile)
            (flip_tile ?tile2)
            ; (not (= ?tile ?tile2))
        )
        :effect (and
            (not (forbidden_tile ?tile))
            (forbidden_tile ?tile2)
        )
    )

)


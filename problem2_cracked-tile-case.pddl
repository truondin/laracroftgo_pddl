(define (problem dummy_cracked_tile)
    (:domain Lara_Croft_Go)
    (:objects
        tile1 - tile
        tile2 - tile
        tile3 - crack_tile
        tile4 - tile
        tile5 - goal

        tile6 - tile
        Lara - agent
    )

    (:init
        (is_crack_tile tile3)

        (path tile1 tile2)
        (path tile2 tile3)
        (path tile3 tile4)
        (path tile4 tile5)

        (path tile2 tile1)
        (path tile3 tile2)
        (path tile4 tile3)
        (path tile5 tile4)

        (path tile6 tile5)
        (fall_path tile3 tile6)

        (at Lara tile1)

    )

    (:goal
        (and
            (at Lara tile5)
            (is_destroyed_tile tile3)
        )
    )
)
(define (problem maze_of_snakes_1a)
    (:domain Lara_Croft_Go)
    (:objects
        1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 32 33 - tile
        31 - goal

        extra_tile - tile

        snake_1 snake_2 snake_3 snake_4 snake_5 snake_6 - snake

        Lara - agent
    )

    (:init
        ; map 
        (path 1 2)

        (path 2 1)
        (path 2 3)

        (path 3 2)
        (path 3 4)

        (path 4 3)
        (path 4 5)

        (path 5 4)
        (path 5 6)

        (path 6 5)
        (path 6 7)

        (path 7 6)
        (path 7 8)

        (path 8 7)
        (path 8 9)

        (path 9 8)
        (path 9 10)

        (path 10 9)
        (path 10 11)

        (path 11 10)
        (path 11 18)
        (path 11 12)

        (path 12 11)
        (path 12 13)

        (path 13 12)
        (path 13 14)

        (path 14 13)
        (path 14 15)

        (path 15 14)
        (path 15 16)
        (path 15 19)

        (path 16 15)
        (path 16 17)

        (path 17 16)
        (path 17 18)

        (path 18 17)
        (path 18 11)

        (path 19 15)
        (path 19 20)

        (path 20 19)
        (path 20 21)
        (path 20 27)

        (path 21 20)
        (path 21 22)

        (path 22 21)
        (path 22 23)

        (path 23 22)
        (path 23 24)

        (path 24 23)
        (path 24 28)
        (path 24 25)

        (path 25 24)
        (path 25 26)

        (path 26 25)
        (path 26 27)

        (path 27 26)
        (path 27 20)

        (path 28 24)
        (path 28 29)

        (path 29 28)
        (path 29 30)
        (path 29 32)

        (path 30 31)
        (path 30 29)

        (path 31 30)

        (path 32 29)
        (path 32 33)

        (path 33 32)

        ;special path

        ;special tiles

        
        ;levers

        ; traps
        (at snake_1 4)
        (trap_guards_tile snake_1 5)
        ; (is_guarded_tile 5)

        
        (at snake_2 8)
        (trap_guards_tile snake_2 extra_tile) ; for having the preconditions, otherwise not finding a plan
        ; (is_guarded_tile extra_tile)

        (at snake_3 13)
        (trap_guards_tile snake_3 14)
        ; (is_guarded_tile 14)

        (at snake_4 17)
        (trap_guards_tile snake_4 18)
        ; (is_guarded_tile 18)

        (at snake_5 23)
        (trap_guards_tile snake_5 24)
        ; (is_guarded_tile 24)

        (at snake_6 25)
        (trap_guards_tile snake_1 26)
        ; (is_guarded_tile 26)

        
        ; agent
        (at Lara 1)
    )

    (:goal
        (and
            (at Lara 31)
        )
    )
)
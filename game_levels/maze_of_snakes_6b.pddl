(define (problem maze_of_snakes_6b)
    (:domain Lara_Croft_Go)
    (:objects
        1 2 3 4 5 6 7 8 9 11 12 13 14 15 16 17 extra_tile_1 extra_tile_2  - tile
        10 18 - crack_tile
        19 - goal

        spear_1 - spear
        snake_1 snake_2 snake_3 snake_4 snake_5 - snake

        Lara - agent
    )

    (:init
        ; map 
        (path 1 2)

        (path 2 1)
        (path 2 3)
        (path 2 6)

        (path 3 2)
        (path 3 4)

        (path 4 3)
        (path 4 5)

        (path 5 4)

        (path 6 2)
        (path 6 7)

        (path 7 6)
        (path 7 8)

        (path 8 7)
        (path 8 9)
        (path 8 18)
        (path 8 10)

        (path 9 8)

        (path 10 8)
        (path 10 11)

        (path 11 10)
        (path 11 12)

        (path 12 11)
        (path 12 13)
        (path 12 19)

        (path 13 12)
        (path 13 14)

        (path 14 13)
        (path 14 15)

        (path 15 14)
        (path 15 16)

        (path 16 15)
        (path 16 17)

        (path 17 16)
        (path 17 18)

        (path 18 17)
        (path 18 8)

        (path 19 12)
        ;special path
        (spear_throw_line 10 9)
        (spear_throw_line 8 11)
        (spear_throw_line 18 16)
        (spear_throw_line 11 13)
        (spear_throw_line 13 11)

        ;special tiles
        (is_crack_tile 10)
        (is_crack_tile 18)
        
        ;levers

        ; traps
        (at snake_1 4)
        (trap_guards_tile snake_1 5)

        (at snake_2 9)
        (trap_guards_tile snake_2 extra_tile_1)

        (at snake_3 11)
        (trap_guards_tile snake_3 12)

        (at snake_4 13)
        (trap_guards_tile snake_4 12)

        (at snake_5 16)
        (trap_guards_tile snake_4 extra_tile_2)        
        ; spears
        (at spear_1 5)

        ; agent
        (at Lara 1)
    )

    (:goal
        (and
            (at Lara 19)
        
        )
    )
)
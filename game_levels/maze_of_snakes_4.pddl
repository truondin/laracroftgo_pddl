(define (problem maze_of_snakes_4)
    (:domain Lara_Croft_Go)
    (:objects
        1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 - tile
        37 - goal

        33 32 27 28 29 30 - assigned_location
        21 23 24 - assigned_location
        20 19 17 18 - assigned_location
        4 6 8 10 9 7 - assigned_location

        lever_orange - lever
        saw_blue saw_green saw_pink saw_red - saw

        Lara - agent
    )

    (:init
        ; map 
        (path 1 2)

        (path 2 1)
        (path 2 3)

        (path 3 2)
        (path 3 4)
        (path 3 5)

        (path 4 3)
        (path 4 6)

        (path 5 3)
        (path 5 6)
        (path 5 7)

        (path 6 5)
        (path 6 4)
        (path 6 8)

        (path 7 5)
        (path 7 8)
        (path 7 9)

        (path 8 7)
        (path 8 6)
        (path 8 10)

        (path 9 7)
        (path 9 10)
        (path 9 11)

        (path 10 8)
        (path 10 9)
        (path 10 12)

        (path 11 9)
        (path 11 12)

        (path 12 11)
        (path 12 10)
        (path 12 13)

        (path 13 12)
        (path 13 14)

        (path 14 13)
        (path 14 15)

        (path 15 14)
        (path 15 16)

        (path 16 15)
        (path 16 17)

        (path 17 16)
        (path 17 18)
        (path 17 19)

        (path 18 17)
        (path 18 20)

        (path 19 17)
        (path 19 20)
        (path 19 21)

        (path 20 18)
        (path 20 19)
        (path 20 22)

        (path 21 19)
        (path 21 22)
        (path 21 23)

        (path 22 20)
        (path 22 21)
        (path 22 24)

        (path 23 21)
        (path 23 24)

        (path 24 22)
        (path 24 23)
        (path 24 25)

        (path 25 24)
        (path 25 26)
        (path 25 34)

        (path 26 25)
        (path 26 33)
        (path 26 27)

        (path 27 26)
        (path 27 32)
        (path 27 28)

        (path 28 27)
        (path 28 29)
        (path 28 31)

        (path 29 28)
        (path 29 30)

        (path 30 29)
        (path 30 31)

        (path 31 30)
        (path 31 28)
        (path 31 32)

        (path 32 31)
        (path 32 33)
        (path 32 27)

        (path 33 32)
        (path 33 34)
        (path 33 26)

        (path 34 33)
        (path 34 25)
        (path 34 35)

        (path 35 34)
        (path 35 36)

        (path 36 35)
        (path 36 37)

        (path 37 36)
      
        ;special path

        ;special tiles
        (forbidden_tile 36)
        
        ;levers
        (at lever_orange 31)
        (lever_activates lever_orange 36)
       
        ; traps
        (at saw_blue 28)
        (trap_guards_tile saw_blue 29)

        (= (move_trap_location saw_blue 33) 1)
        (= (guarded_tile_location_increase saw_blue 32) 1)
        (= (guarded_tile_location_decrease 30 saw_blue) 1000)

        (= (move_trap_location saw_blue 32) 2)
        (= (guarded_tile_location_increase saw_blue 27) 2)
        (= (guarded_tile_location_decrease 33 saw_blue) 2)

        (= (move_trap_location saw_blue 27) 3)
        (= (guarded_tile_location_increase saw_blue 28) 3)
        (= (guarded_tile_location_decrease 32 saw_blue) 3)
                
        (= (move_trap_location saw_blue 28) 4)
        (= (guarded_tile_location_increase saw_blue 29) 4)
        (= (guarded_tile_location_decrease 27 saw_blue) 4)

        (= (move_trap_location saw_blue 29) 5)
        (= (guarded_tile_location_increase saw_blue 30) 5)
        (= (guarded_tile_location_decrease 28 saw_blue) 5)

        (= (move_trap_location saw_blue 30) 6)
        (= (guarded_tile_location_increase saw_blue 33) 6000)
        (= (guarded_tile_location_decrease 29 saw_blue) 6)
    
        (= (move_trap_step_count saw_blue) 5)
        (= (move_trap_step_upper_limit saw_blue) 6) 
        (= (move_trap_step_lower_limit saw_blue) 1)
        (do_increase saw_blue)
    

        (= (move_trap_location saw_blue 21) 6001)
        (= (move_trap_location saw_blue 23) 6002)
        (= (move_trap_location saw_blue 24) 6003)
        (= (move_trap_location saw_blue 20) 6004)
        (= (move_trap_location saw_blue 19) 6005)
        (= (move_trap_location saw_blue 17) 6006)
        (= (move_trap_location saw_blue 18) 6007)
        (= (move_trap_location saw_blue 4) 6008)
        (= (move_trap_location saw_blue 6) 6009)
        (= (move_trap_location saw_blue 8) 6010)
        (= (move_trap_location saw_blue 10) 6011)
        (= (move_trap_location saw_blue 9) 6012)
        (= (move_trap_location saw_blue 7) 6013)
        

        (= (guarded_tile_location_increase saw_blue 21) 6001)
        (= (guarded_tile_location_increase saw_blue 23) 6002)
        (= (guarded_tile_location_increase saw_blue 24) 6003)
        (= (guarded_tile_location_increase saw_blue 20) 6004)
        (= (guarded_tile_location_increase saw_blue 19) 6005)
        (= (guarded_tile_location_increase saw_blue 17) 6006)
        (= (guarded_tile_location_increase saw_blue 18) 6007)
        (= (guarded_tile_location_increase saw_blue 4) 6008)
        (= (guarded_tile_location_increase saw_blue 6) 6009)
        (= (guarded_tile_location_increase saw_blue 8) 6010)
        (= (guarded_tile_location_increase saw_blue 10) 6011)
        (= (guarded_tile_location_increase saw_blue 9) 6012)
        (= (guarded_tile_location_increase saw_blue 7) 6013)

        (= (guarded_tile_location_decrease 21 saw_blue) 1001)
        (= (guarded_tile_location_decrease 23 saw_blue) 1002)
        (= (guarded_tile_location_decrease 24 saw_blue) 1003)
        (= (guarded_tile_location_decrease 20 saw_blue) 1004)
        (= (guarded_tile_location_decrease 19 saw_blue) 1005)
        (= (guarded_tile_location_decrease 17 saw_blue) 1006)
        (= (guarded_tile_location_decrease 18 saw_blue) 1007)
        (= (guarded_tile_location_decrease 4 saw_blue) 1008)
        (= (guarded_tile_location_decrease 6 saw_blue) 1009)
        (= (guarded_tile_location_decrease 8 saw_blue) 1010)
        (= (guarded_tile_location_decrease 10 saw_blue) 1011)
        (= (guarded_tile_location_decrease 9 saw_blue) 1012)
        (= (guarded_tile_location_decrease 7 saw_blue) 1013)

        (at saw_green 23)
        (trap_guards_tile saw_green 24)

        (= (move_trap_location saw_green 21) 1)
        (= (guarded_tile_location_increase saw_green 23) 1)
        (= (guarded_tile_location_decrease 24 saw_green) 1000)

        (= (move_trap_location saw_green 23) 2)
        (= (guarded_tile_location_increase saw_green 24) 2)
        (= (guarded_tile_location_decrease 21 saw_green) 2)

        (= (move_trap_location saw_green 24) 3)
        (= (guarded_tile_location_increase saw_green 21) 3000)
        (= (guarded_tile_location_decrease 23 saw_green) 3)

        (= (move_trap_step_count saw_green) 3)
        (= (move_trap_step_upper_limit saw_green) 3) 
        (= (move_trap_step_lower_limit saw_green) 1) 

        (= (move_trap_location saw_green 33) 1004)
        (= (move_trap_location saw_green 32) 1005)
        (= (move_trap_location saw_green 27) 1006)
        (= (move_trap_location saw_green 28) 1007)
        (= (move_trap_location saw_green 29) 1008)
        (= (move_trap_location saw_green 30) 1009)
        (= (move_trap_location saw_green 20) 1010)
        (= (move_trap_location saw_green 19) 1011)
        (= (move_trap_location saw_green 17) 1012)
        (= (move_trap_location saw_green 18) 1013)
        (= (move_trap_location saw_green 4) 1014)
        (= (move_trap_location saw_green 6) 1015)
        (= (move_trap_location saw_green 8) 1016)
        (= (move_trap_location saw_green 10) 1017)
        (= (move_trap_location saw_green 9) 1017)
        (= (move_trap_location saw_green 7) 1018)

        (= (guarded_tile_location_increase saw_green 33) 3001)
        (= (guarded_tile_location_increase saw_green 32) 3002)
        (= (guarded_tile_location_increase saw_green 27) 3003)
        (= (guarded_tile_location_increase saw_green 28) 3004)
        (= (guarded_tile_location_increase saw_green 29) 3005)
        (= (guarded_tile_location_increase saw_green 30) 3006)
        (= (guarded_tile_location_increase saw_green 20) 3007)
        (= (guarded_tile_location_increase saw_green 19) 3008)
        (= (guarded_tile_location_increase saw_green 17) 3009)
        (= (guarded_tile_location_increase saw_green 18) 3010)
        (= (guarded_tile_location_increase saw_green 4) 3011)
        (= (guarded_tile_location_increase saw_green 6) 3012)
        (= (guarded_tile_location_increase saw_green 8) 3013)
        (= (guarded_tile_location_increase saw_green 10) 3014)
        (= (guarded_tile_location_increase saw_green 9) 3015)
        (= (guarded_tile_location_increase saw_green 7) 3016)

        (= (guarded_tile_location_decrease 33 saw_green) 1004)
        (= (guarded_tile_location_decrease 32 saw_green) 1005)
        (= (guarded_tile_location_decrease 27 saw_green) 1006)
        (= (guarded_tile_location_decrease 28 saw_green) 1007)
        (= (guarded_tile_location_decrease 29 saw_green) 1008)
        (= (guarded_tile_location_decrease 30 saw_green) 1009)
        (= (guarded_tile_location_decrease 20 saw_green) 1010)
        (= (guarded_tile_location_decrease 19 saw_green) 1011)
        (= (guarded_tile_location_decrease 17 saw_green) 1012)
        (= (guarded_tile_location_decrease 18 saw_green) 1013)
        (= (guarded_tile_location_decrease 4 saw_green) 1014)
        (= (guarded_tile_location_decrease 6 saw_green) 1015)
        (= (guarded_tile_location_decrease 8 saw_green) 1016)
        (= (guarded_tile_location_decrease 10 saw_green) 1017)
        (= (guarded_tile_location_decrease 9 saw_green) 1018)
        (= (guarded_tile_location_decrease 7 saw_green) 1019)

        (at saw_pink 19)
        (trap_guards_tile saw_pink 17)

        (= (move_trap_location saw_pink 20) 1)
        (= (guarded_tile_location_increase saw_pink 19) 1)
        (= (guarded_tile_location_decrease 18 saw_pink) 1000)

        (= (move_trap_location saw_pink 19) 2)
        (= (guarded_tile_location_increase saw_pink 17) 2)
        (= (guarded_tile_location_decrease 20 saw_pink) 2)

        (= (move_trap_location saw_pink 17) 3)
        (= (guarded_tile_location_increase saw_pink 18) 3)
        (= (guarded_tile_location_decrease 19 saw_pink) 3)

        (= (move_trap_location saw_pink 18) 4)
        (= (guarded_tile_location_increase saw_pink 20) 4000)
        (= (guarded_tile_location_decrease 17 saw_pink) 4)
        
        (= (move_trap_step_count saw_pink) 3)
        (= (move_trap_step_upper_limit saw_pink) 4) 
        (= (move_trap_step_lower_limit saw_pink) 1)
        (do_increase saw_pink)

        (= (move_trap_location saw_pink 33) 1004)
        (= (move_trap_location saw_pink 32) 1005)
        (= (move_trap_location saw_pink 27) 1006)
        (= (move_trap_location saw_pink 28) 1007)
        (= (move_trap_location saw_pink 29) 1008)
        (= (move_trap_location saw_pink 30) 1009)
        (= (move_trap_location saw_pink 24) 1010)
        (= (move_trap_location saw_pink 23) 1011)
        (= (move_trap_location saw_pink 21) 1012)
        (= (move_trap_location saw_pink 4) 1013)
        (= (move_trap_location saw_pink 6) 1014)
        (= (move_trap_location saw_pink 8) 1015)
        (= (move_trap_location saw_pink 10) 1016)
        (= (move_trap_location saw_pink 9) 1017)
        (= (move_trap_location saw_pink 7) 1018)

        (= (guarded_tile_location_increase saw_pink 33) 4001)
        (= (guarded_tile_location_increase saw_pink 32) 4002)
        (= (guarded_tile_location_increase saw_pink 27) 4003)
        (= (guarded_tile_location_increase saw_pink 28) 4004)
        (= (guarded_tile_location_increase saw_pink 29) 4005)
        (= (guarded_tile_location_increase saw_pink 30) 4006)
        (= (guarded_tile_location_increase saw_pink 24) 4007)
        (= (guarded_tile_location_increase saw_pink 23) 4008)
        (= (guarded_tile_location_increase saw_pink 21) 4009)
        (= (guarded_tile_location_increase saw_pink 4) 4010)
        (= (guarded_tile_location_increase saw_pink 6) 4011)
        (= (guarded_tile_location_increase saw_pink 8) 4012)
        (= (guarded_tile_location_increase saw_pink 10) 4013)
        (= (guarded_tile_location_increase saw_pink 9) 4014)
        (= (guarded_tile_location_increase saw_pink 7) 4015)

        (= (guarded_tile_location_decrease 33 saw_pink) 1001)
        (= (guarded_tile_location_decrease 32 saw_pink) 1002)
        (= (guarded_tile_location_decrease 27 saw_pink) 1003)
        (= (guarded_tile_location_decrease 28 saw_pink) 1004)
        (= (guarded_tile_location_decrease 29 saw_pink) 1005)
        (= (guarded_tile_location_decrease 30 saw_pink) 1006)
        (= (guarded_tile_location_decrease 24 saw_pink) 1007)
        (= (guarded_tile_location_decrease 23 saw_pink) 1008)
        (= (guarded_tile_location_decrease 21 saw_pink) 1009)
        (= (guarded_tile_location_decrease 4 saw_pink) 1010)
        (= (guarded_tile_location_decrease 6 saw_pink) 1011)
        (= (guarded_tile_location_decrease 8 saw_pink) 1012)
        (= (guarded_tile_location_decrease 10 saw_pink) 1013)
        (= (guarded_tile_location_decrease 9 saw_pink) 1014)
        (= (guarded_tile_location_decrease 7 saw_pink) 1015)

        (at saw_red 8)
        (trap_guards_tile saw_red 10)

        (= (move_trap_location saw_red 4) 1)
        (= (guarded_tile_location_increase saw_red 6) 1)
        (= (guarded_tile_location_decrease 7 saw_red) 1000)

        (= (move_trap_location saw_red 6) 2)
        (= (guarded_tile_location_increase saw_red 8) 2)
        (= (guarded_tile_location_decrease 4 saw_red) 2)

        (= (move_trap_location saw_red 8) 3)
        (= (guarded_tile_location_increase saw_red 10) 3)
        (= (guarded_tile_location_decrease 6 saw_red) 3)

        (= (move_trap_location saw_red 10) 4)
        (= (guarded_tile_location_increase saw_red 9) 4)
        (= (guarded_tile_location_decrease 8 saw_red) 4)

        (= (move_trap_location saw_red 9) 5)
        (= (guarded_tile_location_increase saw_red 7) 5)
        (= (guarded_tile_location_decrease 10 saw_red) 5)

        (= (move_trap_location saw_red 7) 6)
        (= (guarded_tile_location_increase saw_red 4) 6000)
        (= (guarded_tile_location_decrease 9 saw_red) 6)

        (= (move_trap_step_count saw_red) 4)
        (= (move_trap_step_upper_limit saw_red) 6) 
        (= (move_trap_step_lower_limit saw_red) 1)
        (do_increase saw_red)

        (= (move_trap_location saw_red 18) 6001)
        (= (move_trap_location saw_red 17) 6002)
        (= (move_trap_location saw_red 19) 6003)
        (= (move_trap_location saw_red 20) 6004)
        (= (move_trap_location saw_red 21) 6005)
        (= (move_trap_location saw_red 23) 6006)
        (= (move_trap_location saw_red 24) 6007)
        (= (move_trap_location saw_red 30) 6008)
        (= (move_trap_location saw_red 29) 6009)
        (= (move_trap_location saw_red 28) 6010)
        (= (move_trap_location saw_red 27) 6011)
        (= (move_trap_location saw_red 32) 6012)
        (= (move_trap_location saw_red 33) 6013)
        
        
        (= (guarded_tile_location_increase saw_red 18) 6001)
        (= (guarded_tile_location_increase saw_red 17) 6002)
        (= (guarded_tile_location_increase saw_red 19) 6003)
        (= (guarded_tile_location_increase saw_red 20) 6004)
        (= (guarded_tile_location_increase saw_red 21) 6005)
        (= (guarded_tile_location_increase saw_red 23) 6006)
        (= (guarded_tile_location_increase saw_red 24) 6007)
        (= (guarded_tile_location_increase saw_red 30) 6008)
        (= (guarded_tile_location_increase saw_red 29) 6009)
        (= (guarded_tile_location_increase saw_red 28) 6010)
        (= (guarded_tile_location_increase saw_red 27) 6011)
        (= (guarded_tile_location_increase saw_red 32) 6012)
        (= (guarded_tile_location_increase saw_red 33) 6013)
        
        (= (guarded_tile_location_decrease 18 saw_red) 1001)
        (= (guarded_tile_location_decrease 17 saw_red) 1002)
        (= (guarded_tile_location_decrease 19 saw_red) 1003)
        (= (guarded_tile_location_decrease 20 saw_red) 1004)
        (= (guarded_tile_location_decrease 21 saw_red) 1005)
        (= (guarded_tile_location_decrease 23 saw_red) 1006)
        (= (guarded_tile_location_decrease 24 saw_red) 1007)
        (= (guarded_tile_location_decrease 30 saw_red) 1008)
        (= (guarded_tile_location_decrease 29 saw_red) 1009)
        (= (guarded_tile_location_decrease 28 saw_red) 1010)
        (= (guarded_tile_location_decrease 27 saw_red) 1011)
        (= (guarded_tile_location_decrease 32 saw_red) 1012)
        (= (guarded_tile_location_decrease 33 saw_red) 1013)

        ; spears


        ; agent
        (at Lara 1)
    )

    (:goal
        (and
            (at Lara 37)
        )
    )
)
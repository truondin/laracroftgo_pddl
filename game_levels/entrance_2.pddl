(define (problem entrance_2)
    (:domain Lara_Croft_Go)
    (:objects
        1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 - tile
        21 - goal

        lever_green lever_yellow lever_blue lever_red - lever
        Lara - agent
    )

    (:init
        ; map 
        (path 1 2)
        (path 2 1)

        (path 2 1)
        (path 2 3)
        (path 2 5)

        (path 3 2)
        (path 3 4)

        (path 4 3)

        (path 5 2)
        (path 5 6)

        (path 6 5)
        (path 6 7)

        (path 7 6)
        (path 7 8)

        (path 8 7)
        (path 8 9)
        
        (path 9 8)
        (path 9 10)
        (path 9 14)

        (path 10 9)
        (path 10 11)

        (path 11 10)
        (path 11 12)

        (path 12 11)
        (path 12 13)

        (path 13 12)

        (path 14 9)
        (path 14 15)

        (path 15 14)
        (path 15 16)
        (path 15 18)

        (path 16 15)
        (path 16 17)

        (path 17 16)

        (path 18 15)
        (path 18 19)

        (path 19 18)
        (path 19 20)

        (path 20 19)
        (path 20 21)

        (path 21 20)

        ;special tiles
        (forbidden_tile 5)
        (forbidden_tile 6)
        (forbidden_tile 12)
        (flip_tile 12)
        (flip_tile 14)
        (forbidden_tile 16)
        (forbidden_tile 18)
        (forbidden_tile 19)
        (forbidden_tile 20)


        ;levers
        (at lever_green 4)
        (lever_activates lever_green 5)
        (lever_activates lever_green 6)
        
        (at lever_yellow 11)
        (lever_activates lever_yellow 12)
        (lever_activates lever_yellow 14)

        (at lever_blue 13)
        (lever_activates lever_blue 16)

        (at lever_red 17)
        (lever_activates lever_red 18)
        (lever_activates lever_red 19)
        (lever_activates lever_red 20)

        ; agent
        (at Lara 1)

    )

    (:goal
        (and
            (at Lara 21)
        )
    )
)
(define (problem maze_of_snakes_6a)
    (:domain Lara_Croft_Go)
    (:objects
        1 2 3 4 5 6 7 8 9 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 - tile
        10 - crack_tile
        27 - goal

        spear_1 spear_2 - spear
        snake_1 snake_2 snake_3 - snake

        Lara - agent
    )

    (:init
        ; map 
        (path 1 2)

        (path 2 1)
        (path 2 3)

        (path 3 2)
        (path 3 4)
        (path 3 12)

        (path 4 3)
        (path 4 5)

        (path 5 4)
        (path 5 6)

        (path 6 5)
        (path 6 7)
        (path 6 11)

        (path 7 6)
        (path 7 8)

        (path 8 7)
        (path 8 9)

        (path 9 8)
        (path 9 10)

        (path 10 9)
        (path 10 11)

        (path 11 10)
        (path 11 6)

        (path 12 3)
        (path 12 13)
        (path 12 21)

        (path 13 12)
        (path 13 14)

        (path 14 13)
        (path 14 15)

        (path 15 14)
        (path 15 16)

        (path 16 15)
        (path 16 17)

        (path 17 16)
        (path 17 18)

        (path 18 17)
        (path 18 19)

        (path 19 18)
        (path 19 20)
        (path 19 22)

        (path 20 19)
        (path 20 21)

        (path 21 20)
        (path 21 12)
        (path 21 23)

        (path 22 19)

        (path 23 21)
        (path 23 24)

        (path 24 23)
        (path 24 25)

        (path 25 24)
        (path 25 26)

        (path 26 25)
        (path 26 27)

        (path 27 26)

        ;special path
        (spear_throw_line 1 21)
        (spear_throw_line 2 21)
        (spear_throw_line 3 21)

        (spear_throw_line 19 24)
        ;special tiles
        (is_crack_tile 10)
        
        ;levers

        ; traps
        (at snake_1 21)
        (trap_guards_tile snake_1 12)

        (at snake_2 19)
        (trap_guards_tile snake_2 20)
        
        (at snake_3 24)
        (trap_guards_tile snake_3 23)

        ; spears
        (at spear_1 9)
        (at spear_2 22)

        ; agent
        (at Lara 1)
    )

    (:goal
        (and
            (at Lara 27)
        
        )
    )
)
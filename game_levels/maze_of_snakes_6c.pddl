(define (problem maze_of_snakes_6c)
    (:domain Lara_Croft_Go)
    (:objects
        1 2 3 4 5 6 7 8 9 11 12 13 14 15 16 17 18  - tile
        10 - goal

        spear_1 - spear
        snake_1 snake_2 snake_3 snake_4 - snake

        Lara - agent
    )

    (:init
        ; map 
        (path 1 2)

        (path 2 1)
        (path 2 3)

        (path 3 2)
        (path 3 11)
        (path 3 4)
        (path 3 15)

        (path 4 3)
        (path 4 12)
        (path 4 5)
        (path 4 16)

        (path 5 4)
        (path 5 13)
        (path 5 6)
        (path 5 17)

        (path 6 5)
        (path 6 7)
        (path 6 14)
        (path 6 18)

        (path 7 6)
        (path 7 8)
        
        (path 8 7)
        (path 8 9)

        (path 9 8)
        (path 9 10)

        (path 10 9)

        (path 11 3)
        (path 11 12)

        (path 12 11)
        (path 12 4)
        (path 12 13)

        (path 13 12)
        (path 13 14)
        (path 13 5)

        (path 14 13)
        (path 14 6)

        (path 15 3)
        (path 15 16)

        (path 16 15)
        (path 16 17)
        (path 16 4)

        (path 17 16)
        (path 17 5)
        (path 17 18)

        (path 18 17)
        (path 18 6)

        ;special path
        (spear_throw_line 15 17)
        (spear_throw_line 3 5)
        (spear_throw_line 4 5)

        ;special tiles
        
        ;levers

        ; traps
        (at snake_1 5)
        (trap_guards_tile snake_1 13)

        (at snake_2 6)
        (trap_guards_tile snake_2 5)

        (at snake_3 17)
        (trap_guards_tile snake_3 16)

        (at snake_4 18)
        (trap_guards_tile snake_4 6)
    
        ; spears
        (at spear_1 3)

        ; agent
        (at Lara 1)
    )

    (:goal
        (and
            (at Lara 10)
        )
    )
)
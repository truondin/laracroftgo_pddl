(define (problem maze_of_snakes_6d)
    (:domain Lara_Croft_Go)
    (:objects
        1 2 3 4 5 6 7 8 9 10 12 13 14 15 16 17 18 19 - tile
        11 - goal

        spear_1 - spear
        snake_1 snake_2 snake_3 snake_4 snake_5 - snake

        Lara - agent
    )

    (:init
        ; map 
        (path 1 2)

        (path 2 1)
        (path 2 3)

        (path 3 2)
        (path 3 12)
        (path 3 4)
        (path 3 16)

        (path 4 3)
        (path 4 13)
        (path 4 5)
        (path 4 17)

        (path 5 4)
        (path 5 14)
        (path 5 6)
        (path 5 18)

        (path 6 5)
        (path 6 7)
        (path 6 15)
        (path 6 19)

        (path 7 6)
        (path 7 8)
        
        (path 8 7)
        (path 8 9)

        (path 9 8)
        (path 9 10)

        (path 10 9)
        (path 10 11)

        (path 11 9)

        (path 12 13)
        (path 12 3)

        (path 13 12)
        (path 13 14)
        (path 13 4)

        (path 14 13)
        (path 14 15)
        (path 14 5)

        (path 15 6)
        (path 15 14)

        (path 16 3)
        (path 16 17)

        (path 17 16)
        (path 17 4)
        (path 17 18)

        (path 18 17)
        (path 18 19)
        (path 18 5)

        (path 19 18)
        (path 19 6)

        ;special path
        (spear_throw_line 12 13)
        (spear_throw_line 3 6)
        (spear_throw_line 4 6)
        (spear_throw_line 18 14)
        (spear_throw_line 4 17)
        (spear_throw_line 4 13)
        (spear_throw_line 19 16)

        ;special tiles
        
        ;levers

        ; traps
        (at snake_1 6)
        (trap_guards_tile snake_1 5)

        (at snake_2 13)
        (trap_guards_tile snake_2 14)

        (at snake_3 14)
        (trap_guards_tile snake_3 13)

        (at snake_4 15)
        (trap_guards_tile snake_4 6)

        (at snake_5 17)
        (trap_guards_tile snake_5 16)
        
        ; spears
        (at spear_1 3)

        ; agent
        (at Lara 1)
    )

    (:goal
        (and
            (at Lara 11)
        )
    )
)
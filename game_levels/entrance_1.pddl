(define (problem maze_of_snakes_6a)
    (:domain Lara_Croft_Go)
    (:objects
        1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17  - tile
        18 - goal

        Lara - agent
    )

    (:init
        ; map 
        (path 1 2)

        (path 2 1)
        (path 2 3)

        (path 3 2)
        (path 3 4)

        (path 4 3)
        (path 4 5)

        (path 5 4)
        (path 5 6)

        (path 6 5)
        (path 6 7)

        (path 7 6)
        (path 7 8)

        (path 8 7)
        (path 8 9)

        (path 9 8)
        (path 9 10)

        (path 10 9)
        (path 10 11)

        (path 11 10)
        (path 11 12)

        (path 12 11)
        (path 12 13)

        (path 13 12)
        (path 13 14)

        (path 14 13)
        (path 14 15)

        (path 15 14)
        (path 15 16)

        (path 16 15)
        (path 16 17)

        (path 17 16)
        (path 17 18)

        (path 18 17)


        ; agent
        (at Lara 1)
    )

    (:goal
        (and
            (at Lara 18)
        
        )
    )
)
(define (problem entrance_3)
    (:domain Lara_Croft_Go)
    (:objects
        1 2 4 5 6 7 8 9 10 12 13 14 15 16 17 18 20 21 22 24 25 26 27 29 - tile
        28 - goal

        3 11 19 23 - crack_tile

        lever_orange - lever
        Lara - agent
    )

    (:init
        ; map 
        (path 1 2)

        (path 2 1)
        (path 2 3)

        (path 3 2)
        (path 3 6)
        (path 3 4)

        (path 4 3)
        (path 4 5)
        
        (path 5 4)
        
        (path 6 3)
        (path 6 7)

        (path 7 6)
        (path 7 8)

        (path 8 7)
        (path 8 9)
        
        (path 9 8)
        (path 9 10)
        (path 9 13)
        
        (path 10 9)
        (path 10 11)

        (path 11 10)
        (path 11 12)

        (path 12 11)
        
        (path 13 9)
        (path 13 14)
        
        (path 14 13)
        (path 14 15)

        (path 15 14)
        (path 15 16)
        (path 15 24)

        (path 16 15)
        (path 16 17)

        (path 17 16)
        (path 17 18)

        (path 18 17)
        (path 18 19)
        (path 18 23)

        (path 19 18)
        (path 19 20)

        (path 20 19)
        (path 20 21)

        (path 21 20)
        (path 21 22)
        
        (path 22 21)
        (path 22 23)
        (path 22 29)
                
        (path 23 22)
        (path 23 18)

        (path 24 15)
        (path 24 25)

        (path 25 24)
        (path 25 26)

        (path 26 25)
        (path 26 27)
        
        (path 27 26)
        (path 27 28)

        (path 28 27)

        (path 29 22)

        ;special path
        (fall_path 11 9)

        ;special tiles
        (is_crack_tile 3)
        (is_crack_tile 11)
        (is_crack_tile 19)
        (is_crack_tile 23)

        (forbidden_tile 13)
        (forbidden_tile 14)
        (flip_tile 13)
        (flip_tile 14)
        (flip_tile 24)
        (flip_tile 25)

        ;levers
        (at lever_orange 12)
        (at lever_orange 21)
        (lever_activates lever_orange 13)
        (lever_activates lever_orange 14)
        (lever_activates lever_orange 24)
        (lever_activates lever_orange 25)

        ; agent
        (at Lara 1)
    )

    (:goal
        (and
            (at Lara 28)
        )
    )
)
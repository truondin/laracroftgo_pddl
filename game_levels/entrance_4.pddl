(define (problem entrance_4)
    (:domain Lara_Croft_Go)
    (:objects
        1 2 3 4 5 6 7 8 9 10 11 13 14 15 16 17 19 20 21 22 24 25 27 28- tile
        12 18 23 - crack_tile
        26 - goal

        lever_orange lever_green - lever

        Lara - agent
    )

    (:init
        ; map 
        (path 1 2)

        (path 2 1)
        (path 2 3)

        (path 3 2)
        (path 3 4)
        
        (path 4 3)
        (path 4 5)

        (path 5 4)
        (path 5 6)
        (path 5 9)
        
        (path 6 5)
        (path 6 7)
        
        (path 7 6)
        (path 7 8)

        (path 8 7)
        
        (path 9 5)
        (path 9 10)
        (path 9 16)

        (path 10 9)
        (path 10 11)

        (path 11 10)
        (path 11 12)
        (path 11 17)
        
        (path 12 11)
        (path 12 13)

        (path 13 12)
        (path 13 14)

        (path 14 13)
        (path 14 15)

        (path 15 14)
        (path 15 16)

        (path 16 15)
        (path 16 9)

        (path 17 11)
        (path 17 18)
        (path 17 20)

        (path 18 17)
        (path 18 19)

        (path 19 18)
        
        (path 20 17)
        (path 20 21)
        
        (path 21 20)
        (path 21 22)

        (path 22 21)
        (path 22 23)

        (path 23 22)
        (path 23 24)

        (path 24 23)
        (path 24 25)

        (path 25 24)
        (path 25 27)

        (path 27 25)
        (path 27 28)

        (path 28 27)
        (path 28 26)
        
        (path 26 28)
        ;special path
        (fall_path 18 16)
        (fall_path 12 16)


        ;special tiles
        (is_crack_tile 12)
        (is_crack_tile 18)
        (is_crack_tile 23)

        (forbidden_tile 16)
        (forbidden_tile 10)
        (flip_tile 5)
        (flip_tile 16)
        (flip_tile 10)
        (flip_tile 21)

        ;levers
        (at lever_orange 8)
        (at lever_orange 13)
        (lever_activates lever_orange 10)
        (lever_activates lever_orange 21)

        (at lever_green 19)
        (lever_activates lever_green 5)
        (lever_activates lever_green 16)

        ; agent
        (at Lara 1)
    )

    (:goal
        (and
            (at Lara 26)
        )
    )
)
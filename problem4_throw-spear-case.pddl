(define (problem dummy_throw_spear)
    (:domain Lara_Croft_Go)
    (:objects
        tile1 tile2 tile3 tile4 tile5  - tile
        goal - goal
        Lara - agent
        snake - snake
        spear - spear
    )

    (:init
        (path tile1 tile2)
        (path tile1 tile3)
        (path tile3 tile4)
        (path tile4 tile5)
        (path tile4 goal)
        
        (path tile2 tile1)
        (path tile3 tile1)
        (path tile4 tile3)
        (path tile5 tile4)
        (path goal tile4)
        
        (spear_throw_line tile2 tile5)
        
        (at spear tile3)
        (at Lara tile1)
        (at snake tile5)
    
        (trap_guards_tile snake tile4)
        ; (is_guarded_tile tile4)
    )

    (:goal
        (and
            (at Lara goal)
        )
    )
)
